package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {

    // Loops
        // are control structures that allow code blocks to be executed multiple times.

    public static void main(String[] args) {

        // While Loop
            // Allows fo repetitive use of code, similar to for loops, but are usually for situations where the content to iterate through is indefinite.

        int x = 0; // Initial value

        while (x<10){ // Condition
            System.out.println("(While Loop) Loop Number: " + x);
            x++;//increment/decrement

        }

        // Do-While Loop
            // Similar to while loops
            // However, do-while loops will always execute at least once - while, while loops may not execute at all

//        int y = 10;
//        do {
//
//            System.out.println("(This will run at least once) Countdown: " + y);
//            y--;
//
//        } while (y>10);

        // For Loop
            // Syntax
        /*
            for (initialVal;condition;iteration){

                // Code Block

            }
        * */


        // Mini-Activity


            for (int i=0; i<10; i++){

                System.out.println("(For Loop) Current Count: " + i);

            }

            // For Loop with Arrays

            int [] intArray = {100,200,300,400,500};
            for (int i=0; i< intArray.length; i++){

                System.out.println(intArray[i]);
            }

            // Like For-Each loop with array

            /*
            * Syntax:
             for (dataType itemName: arrayName){

                    // Code Block

             }
            * */

            String[] boyBandArray = {"John", "Paul", "George", "Ringo"};

            for(String member: boyBandArray){
                System.out.println(member);
            }

            // Multidimensional Array

            String[][] classroom = new String[3][3];
            //[row][column]

            //First row
            classroom[0][0] = "Jenny";
            classroom[0][1] = "Liza";
            classroom[0][2] = "Rose";
            //Second row
            classroom[1][0] = "Ash";
            classroom[1][1] = "Misty";
            classroom[1][2] = "Brock";
            //Third row
            classroom[2][0] = "Amy";
            classroom[2][1] = "Lulu";
            classroom[2][2] = "Morgan";

            for (int row = 0 ; row<3; row++){
                for (int col=0;col<3; col++){
                    System.out.println("Classroom ["+row+"]["+col+"] = " + classroom[row][col]);
                }

            } //  Classroom [0][0] = Jenny

            // For-Each loop with multidimensional array

            for(String[] row: classroom){
                for(String column: row){
                    System.out.println(column);
                }
            } // Jenny

            // Print as a matrix
            for (int row =0; row<classroom.length; row++){
                for(int column = 0; column<classroom[row].length; column++){
                    System.out.print(classroom[row][column] + " ");
                }
                System.out.println();
            }

            // Mini Activity
            // ***
            // 285
            // ***

            String[][] b285Matrix = {{"*","*","*"},{"2","8","5"},{"*","*","*"}};

            for (int row = 0; row<b285Matrix.length; row++) {
                for (int column = 0; column<b285Matrix[row].length ;column++) {
                    System.out.print(b285Matrix[row][column]);
                }
                System.out.println();
            }


        // For-Each for ArrayList
        /*
         arrayListName.forEach(Consumer<E> -> // Code Block);
         "->" This is called the lambda operator - is used to separate parameter and implementation.
        * */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("Array List" + numbers);

        numbers.forEach(num -> System.out.println("Array List " + num));

        // forEach for HashMaps
        /*
        * hashMapName.forEach((key,value) -> // Code Block);
        * */

        HashMap<String, Integer> grades = new HashMap<String, Integer>(){
            {
            this.put("English", 90);
            this.put("Math", 95);
            this.put("Science", 97);
            this.put("History", 94);
            }
        };

        grades.forEach((subject, grade) -> {
            System.out.println("Hashmaps: " + subject + " : "+ grade + "\n");
        });

    }


}
