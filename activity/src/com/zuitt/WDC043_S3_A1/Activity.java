package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;
import java.text.NumberFormat;

public class Activity {

    public static void main(String[] args) {

        NumberFormat myFormat = NumberFormat.getInstance();
        Scanner in = new Scanner(System.in);
        boolean isValid = false;

        while (!isValid){

            // While Loop

            try {
                System.out.println("(While Loop) Please enter a number for factorial computation: ");
                int num = in.nextInt();
                if (num <= 0) {
                    System.out.println("You have entered an invalid integer.");
                } else {
                    System.out.println("You have Entered: " + num);
                    int input = num;
                    int answer = 1;
                    int i = 1;

                    while (i < num) {
                        answer = num * answer;
                        num--;
                    }
                    System.out.println("(While Loop) The factorial of " + input + " is " + myFormat.format(answer) + "\n");
                    isValid = true;
                }


                // For Loop

                System.out.println("(For Loop) Please enter a number for factorial computation: ");
                int num2 = in.nextInt();

                if (num2 <= 0) {
                    System.out.println("You have entered an invalid integer.");
                } else {
                    System.out.println("You have Entered: " + num2);
                    int input2 = num2;
                    int answer = 1;

                    for(int i=1;i<num2; num2--){
                        answer = num2 * answer;
                    }
                    System.out.println("(For Loop) The factorial of " + input2 + " is " + myFormat.format(answer));
                }
            }
            catch (Exception e){
                System.out.println("Invalid Input");
                in.nextLine();
            }
        }

        }
    }

